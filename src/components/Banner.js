// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import {Button, Row, Col} from 'react-bootstrap'
// import Home from './pages/Home'
import {Link} from 'react-router-dom';


export default function Banner(props) {

    return props.isError == false ? (

        <Row>
            <Col className="p-5">
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button variant="primary">Enroll Now!</Button>
            </Col>
        </Row>

    ) 

    : 

    (
        <Row>
            <Col className="p-5">
                <h3>Zuitt Booking</h3>
                <h1>Page Not Found</h1>
                <p>
                    Go back to the{" "}
                    <Link as={Link} to="/">
                        homepage
                    </Link>
                </p>
            </Col>
        </Row>
    );
}