import {Row, Col, Card, Button} from 'react-bootstrap';
// import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';



export default function CourseCard ({courseProp}) {
	// console.log(props)
	// console.log(typeof props)
	// console.log(courseProp)
	const {name, description, price, _id} = courseProp

/*
	Syntax:
            const [getter, setter] = useState(initialGetterValue)
*/

// const [count, setCount] = useState (0)
// const [seat, setSeat] = useState (30)

// function enroll () {
	

// 		setSeat(seat - 1)	
// 		setCount(count + 1)


// }

// useEffect(() =>{

// 	if (seat === 0){
// 		alert("NO more seats available.");
// 	} 
// }, [seat]);




	// console.log('Enrollees' + count)


	return(

		<Row>
			<Col xs={12} md={12}>
				<Card>
					<Card.Body>
			    		<Card.Title>{name}</Card.Title>
			    		<Card.Subtitle>
			    			Description:<br/>
			     			{description}
			     		</Card.Subtitle>
			    		<Card.Text className="mt-3">
			       			Price:<br/>
			       			{price}
			    		</Card.Text>
			    		{/*<Card.Text>Enrollees:{count}</Card.Text>
			    		<Card.Text>Seats available:{seat}</Card.Text>
			    		<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
			    		<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			    	</Card.Body>
				</Card>
			</Col>
		</Row>	
		
		)
}