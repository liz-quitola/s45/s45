import React from 'react';
import Banner from '../components/Banner';

export default function Error(props) {
    
    return <Banner isError={true} />;
}